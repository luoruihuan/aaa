﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AngularJSTest.Startup))]
namespace AngularJSTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
