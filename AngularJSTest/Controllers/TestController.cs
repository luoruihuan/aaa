﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AngularJSTest.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit()
        {
          
            return View();
        }

        public ActionResult TestTable()
        {
            return View();
        }



        public JsonResult GetZLDL(Guid id, string search)
        {
            CKDIS entity = new CKDIS();

            var dmdList = entity.DictionaryManagementDetail.Where(d => d.DictionaryManagementItemID == id).ToList();

            if (!string.IsNullOrEmpty(search))
            {
                dmdList = dmdList.Where(d => d.DetailCode.IndexOf(search) != -1 ).ToList();
            }

            return Json(dmdList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetXBS(Guid pid)
        {
            CKDIS entity = new CKDIS();
            var sbs = entity.HistoryRecord.Where(h => h.PatientID == pid).FirstOrDefault();

            if (sbs != null)
            {
                return Json(sbs, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveXBS(HistoryRecord hr)
        {
            CKDIS entity = new CKDIS();
            var sbs = entity.HistoryRecord.Where(h => h.PatientID == hr.PatientID).FirstOrDefault();
            if (sbs != null)
            {
                sbs.Pathogeny = hr.Pathogeny;
            }

            if (entity.SaveChanges() > 0)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

    }
}