namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SysHospital")]
    public partial class SysHospital
    {
        public Guid ID { get; set; }

        [Required]
        [StringLength(50)]
        public string HospName { get; set; }

        [Required]
        [StringLength(50)]
        public string HospShortName { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        public Guid? Tag { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public bool Enabled { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }
}
