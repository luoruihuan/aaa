namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C_CookBook
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        public int Type { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public int DishID1 { get; set; }

        public int DishID2 { get; set; }

        public int DishID3 { get; set; }

        public int DishID4 { get; set; }

        public decimal Energy { get; set; }

        public decimal PRO { get; set; }

        public decimal Fat { get; set; }

        public decimal Carbohydrate { get; set; }

        public decimal K { get; set; }

        public decimal Na { get; set; }

        public decimal Ca { get; set; }

        public decimal P { get; set; }

        public int StandardWeight { get; set; }

        public double ProRatio { get; set; }

        public int CanDiabetesEat { get; set; }

        public int RowStatus { get; set; }

        public int AddUserID { get; set; }

        [Required]
        [StringLength(50)]
        public string AddUserName { get; set; }

        public DateTime AddTime { get; set; }

        public int UpdateUserID { get; set; }

        [Required]
        [StringLength(50)]
        public string UpdateUserName { get; set; }

        public DateTime UpdateTime { get; set; }
    }
}
