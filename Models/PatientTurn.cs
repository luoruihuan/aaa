namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PatientTurn")]
    public partial class PatientTurn
    {
        public Guid ID { get; set; }

        public Guid PatientID { get; set; }

        public DateTime TurnDate { get; set; }

        public bool TurnStatus { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }

        public bool? Tag { get; set; }

        public Guid? TurnReason { get; set; }
    }
}
