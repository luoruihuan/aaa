namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FollowUpStatistics
    {
        public Guid ID { get; set; }

        public Guid FollowUpRecordID { get; set; }

        public int PhysicalExaminationCount { get; set; }

        public int MedicationRecordCount { get; set; }

        public int DiagnosticRecordCount { get; set; }

        public int PACSDetailRecordCount { get; set; }

        public int InspectionRecordCount { get; set; }

        public int NutritionSurveyDetailCount { get; set; }

        public int NutritionSurveySGACount { get; set; }

        public int FollowUpAssessmentCount { get; set; }

        public int MissionaryDetailRecordCount { get; set; }

        public Guid? Creator { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }
}
