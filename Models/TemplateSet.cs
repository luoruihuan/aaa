namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TemplateSet")]
    public partial class TemplateSet
    {
        public Guid ID { get; set; }

        [StringLength(50)]
        public string TemplateCode { get; set; }

        [Required]
        [StringLength(50)]
        public string TemplateName { get; set; }

        [StringLength(50)]
        public string TemplateShortName { get; set; }

        public int Type { get; set; }

        public int Tag { get; set; }

        public int Tag2 { get; set; }

        public int? TypeStatus { get; set; }

        public Guid? PatientID { get; set; }

        public Guid? SysHospitalID { get; set; }

        public int? Count { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public bool Enabled { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }
}
