namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PACSDetailRecord")]
    public partial class PACSDetailRecord
    {
        public Guid ID { get; set; }

        public Guid PatientId { get; set; }

        public int type { get; set; }

        [StringLength(32)]
        public string ApplicationNo { get; set; }

        [StringLength(32)]
        public string ExamItem { get; set; }

        [StringLength(32)]
        public string ExamPosition { get; set; }

        [StringLength(500)]
        public string ExamDescription { get; set; }

        [StringLength(500)]
        public string ExamResult { get; set; }

        public Guid? Creator { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdateDate { get; set; }

        [StringLength(32)]
        public string PatientNo { get; set; }
    }
}
