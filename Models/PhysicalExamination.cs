namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PhysicalExamination")]
    public partial class PhysicalExamination
    {
        public Guid ID { get; set; }

        public Guid PatientID { get; set; }

        public decimal? Height { get; set; }

        public decimal? Weight { get; set; }

        public int? HeartRate { get; set; }

        [StringLength(50)]
        public string BloodPressure { get; set; }

        public DateTime ExaminationDate { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }

        public decimal? BMI { get; set; }

        public int? TonsilEnlargement { get; set; }

        public bool? Caries { get; set; }

        public bool? Edema { get; set; }

        public bool? Liver { get; set; }

        public bool? Splenomegaly { get; set; }
    }
}
