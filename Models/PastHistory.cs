namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PastHistory")]
    public partial class PastHistory
    {
        public Guid ID { get; set; }

        public Guid PatientID { get; set; }

        [StringLength(200)]
        public string PastRecord { get; set; }

        [StringLength(200)]
        public string ContagionRecord { get; set; }

        [StringLength(200)]
        public string DosageRecord { get; set; }

        [StringLength(200)]
        public string AllergyRecord { get; set; }

        [StringLength(200)]
        public string FamilyRecord { get; set; }

        [StringLength(200)]
        public string SurgicalTraumaRecord { get; set; }

        public bool Smoke { get; set; }

        public bool Smoking { get; set; }

        public int? Amount { get; set; }

        public int? SmokeYear { get; set; }

        public bool? Spirit { get; set; }

        public int? SpiritAmount { get; set; }

        public bool? RedWine { get; set; }

        public int? RedWineAmount { get; set; }

        public bool? Beer { get; set; }

        public int? BeerAmount { get; set; }

        public bool? YellowWine { get; set; }

        public int? YellowWineAmount { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }

        public bool Drink { get; set; }
    }
}
