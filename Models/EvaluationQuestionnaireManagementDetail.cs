namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EvaluationQuestionnaireManagementDetail")]
    public partial class EvaluationQuestionnaireManagementDetail
    {
        public Guid ID { get; set; }

        public Guid EvaluationQuestionnaireManagementItemID { get; set; }

        public int? DetailCode { get; set; }

        [StringLength(500)]
        public string DetailName { get; set; }

        public int Questions { get; set; }

        public int Amount { get; set; }

        [StringLength(50)]
        public string Tag { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }
}
