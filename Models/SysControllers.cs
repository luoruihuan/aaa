namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SysControllers
    {
        public Guid ID { get; set; }

        public int AreaID { get; set; }

        [Required]
        [StringLength(50)]
        public string ControllerCode { get; set; }

        [Required]
        [StringLength(50)]
        public string ControllerDisplayName { get; set; }

        [Required]
        [StringLength(50)]
        public string ControllerName { get; set; }

        public int? Tag { get; set; }

        [StringLength(50)]
        public string IconName { get; set; }

        public Guid? SysHospitalID { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public bool Enabled { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }
}
