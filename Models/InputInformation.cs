namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InputInformation")]
    public partial class InputInformation
    {
        public Guid ID { get; set; }

        public Guid PatientID { get; set; }

        public Guid FollowUpRecordID { get; set; }

        [Required]
        [StringLength(50)]
        public string InpOutNo { get; set; }

        public bool? PACSInfo { get; set; }

        public bool? LISInfo { get; set; }

        public bool? MedicalOrders { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }
}
