namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FollowUpAssessment")]
    public partial class FollowUpAssessment
    {
        public Guid ID { get; set; }

        public Guid QuestionnaireItemID { get; set; }

        public Guid QuestionnaireDetailID { get; set; }

        public Guid QuestionnaireAnswerID { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }

        public Guid? PatientID { get; set; }

        public Guid FollowUpRecordID { get; set; }
    }
}
