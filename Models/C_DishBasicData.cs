namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C_DishBasicData
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        public int IsBreakfast { get; set; }

        public int IsLunch { get; set; }

        public int IsDinner { get; set; }

        public int DishType { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(200)]
        public string ImageUrl { get; set; }

        [Required]
        [StringLength(50)]
        public string Unit { get; set; }

        public decimal Energy { get; set; }

        public decimal PRO { get; set; }

        public decimal Fat { get; set; }

        public decimal Carbohydrate { get; set; }

        public decimal K { get; set; }

        public decimal Na { get; set; }

        public decimal Ca { get; set; }

        public decimal P { get; set; }

        [Required]
        public string Dosing { get; set; }

        [Required]
        public string CreationMethod { get; set; }

        public int CanCKDEat { get; set; }

        public int CanKDEat { get; set; }

        public int CanDiabetesEat { get; set; }

        public int IsLiquid { get; set; }

        public int RowStatus { get; set; }

        public int AddUserID { get; set; }

        [Required]
        [StringLength(50)]
        public string AddUserName { get; set; }

        public DateTime AddTime { get; set; }

        public int UpdateUserID { get; set; }

        [Required]
        [StringLength(50)]
        public string UpdateUserName { get; set; }

        public DateTime UpdateTime { get; set; }
    }
}
