namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NutritionSurveySGADetail")]
    public partial class NutritionSurveySGADetail
    {
        public Guid ID { get; set; }

        public Guid NutritionSurveyItemID { get; set; }

        public Guid PatientID { get; set; }

        public Guid Result { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }

        public Guid QuestionDetailID { get; set; }
    }
}
