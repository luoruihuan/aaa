namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HistoryRecord")]
    public partial class HistoryRecord
    {
        public Guid ID { get; set; }

        public Guid PatientID { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstDiagnosisHospital { get; set; }

        public DateTime FirstDiagnosisDate { get; set; }

        public Guid? Pathogeny { get; set; }

        [Required]
        [StringLength(200)]
        public string Symptom { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }
}
