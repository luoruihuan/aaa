namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DiagnosticRecord")]
    public partial class DiagnosticRecord
    {
        public Guid ID { get; set; }

        public Guid PatientID { get; set; }

        public int? Type { get; set; }

        [StringLength(32)]
        public string OrderNo { get; set; }

        [StringLength(32)]
        public string OrderGroupNo { get; set; }

        [StringLength(32)]
        public string ItemName { get; set; }

        [StringLength(255)]
        public string DrugSpecification { get; set; }

        [StringLength(32)]
        public string DoseWay { get; set; }

        public int? DrugUseOneDosage { get; set; }

        [StringLength(32)]
        public string DrugUseOneDosageUnit { get; set; }

        [StringLength(32)]
        public string DrugUseFrequencyCode { get; set; }

        [StringLength(32)]
        public string Unit { get; set; }

        public decimal? UnitPrice { get; set; }

        public DateTime? OrderDate { get; set; }

        public int? Amout { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }
}
