namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EvaluationQuestionnaireManagementItem")]
    public partial class EvaluationQuestionnaireManagementItem
    {
        public Guid ID { get; set; }

        [StringLength(50)]
        public string ItemCode { get; set; }

        [Required]
        [StringLength(50)]
        public string ItemName { get; set; }

        public int? Type { get; set; }

        public Guid? SysHospitalID { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public bool Enabled { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }

        public int? Tag { get; set; }
    }
}
