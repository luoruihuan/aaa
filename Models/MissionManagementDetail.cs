namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MissionManagementDetail")]
    public partial class MissionManagementDetail
    {
        public Guid ID { get; set; }

        public Guid MissionManagementItemID { get; set; }

        [StringLength(50)]
        public string DetailCode { get; set; }

        [Required]
        [StringLength(50)]
        public string DetailName { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }
}
