namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NutritionSurveyDetail")]
    public partial class NutritionSurveyDetail
    {
        public Guid ID { get; set; }

        public Guid NutritionSurveyItemID { get; set; }

        public Guid PatientID { get; set; }

        public int? Oil { get; set; }

        public int? Salt { get; set; }

        public int? Drinkingwater { get; set; }

        public int? BF_Cereal { get; set; }

        public int? BF_Noodle { get; set; }

        public int? BF_Tubers { get; set; }

        public int? BF_Vegetables { get; set; }

        public int? BF_Meat { get; set; }

        public int? BF_Egg { get; set; }

        public int? BF_Milk { get; set; }

        public int? BF_Nut { get; set; }

        public int? BF_Starch { get; set; }

        public int? L_Cereal { get; set; }

        public int? L_Noodle { get; set; }

        public int? L_Tubers { get; set; }

        public int? L_Vegetables { get; set; }

        public int? L_Meat { get; set; }

        public int? L_Egg { get; set; }

        public int? L_Milk { get; set; }

        public int? L_Nut { get; set; }

        public int? L_Starch { get; set; }

        public int? D_Cereal { get; set; }

        public int? D_Noodle { get; set; }

        public int? D_Tubers { get; set; }

        public int? D_Vegetables { get; set; }

        public int? D_Meat { get; set; }

        public int? D_Egg { get; set; }

        public int? D_Milk { get; set; }

        public int? D_Nut { get; set; }

        public int? D_Starch { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }
}
