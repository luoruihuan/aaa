namespace Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CKDIS : DbContext
    {
        public CKDIS()
            : base("name=CKDIS")
        {
        }

        public virtual DbSet<C_CookBook> C_CookBook { get; set; }
        public virtual DbSet<C_DishBasicData> C_DishBasicData { get; set; }
        public virtual DbSet<DiagnosticRecord> DiagnosticRecord { get; set; }
        public virtual DbSet<DictionaryManagementDetail> DictionaryManagementDetail { get; set; }
        public virtual DbSet<DictionaryManagementItem> DictionaryManagementItem { get; set; }
        public virtual DbSet<EvaluationQuestionnaireManagementAnswer> EvaluationQuestionnaireManagementAnswer { get; set; }
        public virtual DbSet<EvaluationQuestionnaireManagementDetail> EvaluationQuestionnaireManagementDetail { get; set; }
        public virtual DbSet<EvaluationQuestionnaireManagementItem> EvaluationQuestionnaireManagementItem { get; set; }
        public virtual DbSet<FollowUpAssessment> FollowUpAssessment { get; set; }
        public virtual DbSet<FollowUpClosed> FollowUpClosed { get; set; }
        public virtual DbSet<FollowUpRecord> FollowUpRecord { get; set; }
        public virtual DbSet<FollowUpStatistics> FollowUpStatistics { get; set; }
        public virtual DbSet<HistoryRecord> HistoryRecord { get; set; }
        public virtual DbSet<InputInformation> InputInformation { get; set; }
        public virtual DbSet<InspectionRecordDetail> InspectionRecordDetail { get; set; }
        public virtual DbSet<InspectionRecordItem> InspectionRecordItem { get; set; }
        public virtual DbSet<MedicationRecord> MedicationRecord { get; set; }
        public virtual DbSet<MissionaryDetailRecord> MissionaryDetailRecord { get; set; }
        public virtual DbSet<MissionaryMasterRecord> MissionaryMasterRecord { get; set; }
        public virtual DbSet<MissionManagementDetail> MissionManagementDetail { get; set; }
        public virtual DbSet<MissionManagementItem> MissionManagementItem { get; set; }
        public virtual DbSet<NutritionSurveyDetail> NutritionSurveyDetail { get; set; }
        public virtual DbSet<NutritionSurveyItem> NutritionSurveyItem { get; set; }
        public virtual DbSet<NutritionSurveySGADetail> NutritionSurveySGADetail { get; set; }
        public virtual DbSet<PACSDetailRecord> PACSDetailRecord { get; set; }
        public virtual DbSet<PastHistory> PastHistory { get; set; }
        public virtual DbSet<Patient> Patient { get; set; }
        public virtual DbSet<PatientExternalSystem> PatientExternalSystem { get; set; }
        public virtual DbSet<PatientTemplateRecord> PatientTemplateRecord { get; set; }
        public virtual DbSet<PatientTurn> PatientTurn { get; set; }
        public virtual DbSet<PhysicalExamination> PhysicalExamination { get; set; }
        public virtual DbSet<SysControllers> SysControllers { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<SysHospital> SysHospital { get; set; }
        public virtual DbSet<SysRole> SysRole { get; set; }
        public virtual DbSet<SysRoleControllers> SysRoleControllers { get; set; }
        public virtual DbSet<SysUser> SysUser { get; set; }
        public virtual DbSet<TemplateDetail> TemplateDetail { get; set; }
        public virtual DbSet<TemplateItem> TemplateItem { get; set; }
        public virtual DbSet<TemplateSet> TemplateSet { get; set; }
        public virtual DbSet<TemplateSetItem> TemplateSetItem { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<C_CookBook>()
                .Property(e => e.Energy)
                .HasPrecision(8, 2);

            modelBuilder.Entity<C_CookBook>()
                .Property(e => e.PRO)
                .HasPrecision(8, 2);

            modelBuilder.Entity<C_CookBook>()
                .Property(e => e.Fat)
                .HasPrecision(8, 2);

            modelBuilder.Entity<C_CookBook>()
                .Property(e => e.Carbohydrate)
                .HasPrecision(8, 2);

            modelBuilder.Entity<C_CookBook>()
                .Property(e => e.K)
                .HasPrecision(8, 2);

            modelBuilder.Entity<C_CookBook>()
                .Property(e => e.Na)
                .HasPrecision(8, 2);

            modelBuilder.Entity<C_CookBook>()
                .Property(e => e.Ca)
                .HasPrecision(8, 2);

            modelBuilder.Entity<C_CookBook>()
                .Property(e => e.P)
                .HasPrecision(8, 2);

            modelBuilder.Entity<C_DishBasicData>()
                .Property(e => e.Energy)
                .HasPrecision(8, 2);

            modelBuilder.Entity<C_DishBasicData>()
                .Property(e => e.PRO)
                .HasPrecision(8, 2);

            modelBuilder.Entity<C_DishBasicData>()
                .Property(e => e.Fat)
                .HasPrecision(8, 2);

            modelBuilder.Entity<C_DishBasicData>()
                .Property(e => e.Carbohydrate)
                .HasPrecision(8, 2);

            modelBuilder.Entity<C_DishBasicData>()
                .Property(e => e.K)
                .HasPrecision(8, 2);

            modelBuilder.Entity<C_DishBasicData>()
                .Property(e => e.Na)
                .HasPrecision(8, 2);

            modelBuilder.Entity<C_DishBasicData>()
                .Property(e => e.Ca)
                .HasPrecision(8, 2);

            modelBuilder.Entity<C_DishBasicData>()
                .Property(e => e.P)
                .HasPrecision(8, 2);

            modelBuilder.Entity<PatientExternalSystem>()
                .Property(e => e.PatientNo)
                .IsUnicode(false);
        }
    }
}
