namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InspectionRecordItem")]
    public partial class InspectionRecordItem
    {
        public Guid ID { get; set; }

        public Guid PatientID { get; set; }

        [StringLength(32)]
        public string ApplicationNo { get; set; }

        [StringLength(32)]
        public string PatientNo { get; set; }

        [StringLength(32)]
        public string Type { get; set; }

        [StringLength(32)]
        public string SampleNo { get; set; }

        [StringLength(32)]
        public string SampleType { get; set; }

        [Required]
        [StringLength(200)]
        public string Item { get; set; }

        public DateTime ApplicationDate { get; set; }

        [StringLength(32)]
        public string ApplicationDepartment { get; set; }

        [StringLength(32)]
        public string ApplicationDoctor { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }
}
