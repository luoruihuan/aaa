namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MissionaryMasterRecord")]
    public partial class MissionaryMasterRecord
    {
        public Guid ID { get; set; }

        public Guid PatientID { get; set; }

        [StringLength(50)]
        public string UploadFile { get; set; }

        public DateTime? UploadDate { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }
}
