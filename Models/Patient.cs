namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Patient")]
    public partial class Patient
    {
        public Guid ID { get; set; }

        [StringLength(50)]
        public string FollowUpId { get; set; }

        [StringLength(18)]
        public string IDCard { get; set; }

        public int? PaymentMethod { get; set; }

        [StringLength(50)]
        public string PaymentNo { get; set; }

        [StringLength(50)]
        public string InpNo { get; set; }

        [StringLength(50)]
        public string OutNo { get; set; }

        public int Tag { get; set; }

        [StringLength(50)]
        public string NationalityCode { get; set; }

        public int Stage { get; set; }

        public bool Status { get; set; }

        public bool Sex { get; set; }

        public int Age { get; set; }

        [Required]
        [StringLength(50)]
        public string PatientName { get; set; }

        public DateTime Birthday { get; set; }

        [StringLength(50)]
        public string Occupation { get; set; }

        public int? MaritalStatus { get; set; }

        [StringLength(200)]
        public string HomeAddress { get; set; }

        [StringLength(50)]
        public string Mobile1 { get; set; }

        [StringLength(50)]
        public string Mobile2 { get; set; }

        [StringLength(50)]
        public string ContactPhone { get; set; }

        [StringLength(50)]
        public string ContactName { get; set; }

        [StringLength(50)]
        public string Relationship { get; set; }

        [StringLength(200)]
        public string ContactAddress { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(50)]
        public string PatientPicture { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public bool Enabled { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }

        public int Type { get; set; }

        public bool? ShowAKI { get; set; }

        [StringLength(20)]
        public string Nation { get; set; }

        public bool? ShowIGA { get; set; }
    }
}
