namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InspectionRecordDetail")]
    public partial class InspectionRecordDetail
    {
        public Guid ID { get; set; }

        public Guid InspectionRecordItemID { get; set; }

        [StringLength(32)]
        public string Code { get; set; }

        [StringLength(32)]
        public string Name { get; set; }

        [StringLength(32)]
        public string Result { get; set; }

        [StringLength(32)]
        public string ResultUnit { get; set; }

        [StringLength(32)]
        public string ReferenceRange { get; set; }

        [StringLength(32)]
        public string NormalFlag { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }
}
