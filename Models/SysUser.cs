namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SysUser")]
    public partial class SysUser
    {
        public Guid ID { get; set; }

        [Required]
        [StringLength(50)]
        public string UserName { get; set; }

        [Required]
        [StringLength(50)]
        public string UserDisplayName { get; set; }

        [Required]
        [StringLength(50)]
        public string Password { get; set; }

        public Guid SysRoleID { get; set; }

        public Guid SysHospitalID { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public bool Enabled { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }
}
