namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PatientExternalSystem")]
    public partial class PatientExternalSystem
    {
        public Guid ID { get; set; }

        public Guid PatientID { get; set; }

        [StringLength(50)]
        public string SystemType { get; set; }

        public int? PatientType { get; set; }

        [StringLength(32)]
        public string PatientNo { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }
}
