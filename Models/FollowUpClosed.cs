namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FollowUpClosed")]
    public partial class FollowUpClosed
    {
        public Guid ID { get; set; }

        public Guid PatientID { get; set; }

        public Guid PTID { get; set; }

        public int Type { get; set; }

        [StringLength(500)]
        public string Reason { get; set; }

        public int? Tag { get; set; }

        public int? Tag2 { get; set; }

        public DateTime DateTime { get; set; }

        [StringLength(50)]
        public string HospitalName { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }
}
