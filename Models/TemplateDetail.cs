namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TemplateDetail")]
    public partial class TemplateDetail
    {
        public Guid ID { get; set; }

        public Guid TemplateItemID { get; set; }

        public Guid? CorrelationID { get; set; }

        [StringLength(50)]
        public string DetailCode { get; set; }

        [StringLength(50)]
        public string DetailName { get; set; }

        public int DisplayForm { get; set; }

        [StringLength(32)]
        public string DetailUnit { get; set; }

        [StringLength(200)]
        public string Range { get; set; }

        public int? Sources { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }
}
