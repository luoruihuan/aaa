namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FollowUpRecord")]
    public partial class FollowUpRecord
    {
        public Guid ID { get; set; }

        public Guid PatientID { get; set; }

        public Guid? TemplateID { get; set; }

        public Guid? SpecialTemplateID { get; set; }

        public Guid? PersonalTemplateID { get; set; }

        public DateTime AppointmentDate { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int Status { get; set; }

        public int Tag { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }

        public int Type { get; set; }
    }
}
