namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EvaluationQuestionnaireManagementAnswer")]
    public partial class EvaluationQuestionnaireManagementAnswer
    {
        public Guid ID { get; set; }

        public Guid EvaluationQuestionnaireManagementDetailID { get; set; }

        public int Sort { get; set; }

        [Required]
        [StringLength(200)]
        public string Answer { get; set; }

        public int Score { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public Guid? Creator { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }

        [StringLength(20)]
        public string IsRight { get; set; }
    }
}
